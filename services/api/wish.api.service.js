import HttpService from "~/services/http.service";

export default class WishApiService {

    static async addWish({title, description, price, category, view_access, currency}) {
        return HttpService.post('wishes/', {
            title,
            description,
            price,
            category,
            view_access,
            currency
        });
    }

    static async getWish({id}) {
        return HttpService.get(`wishes/${id}/`, )
    }

    static async putWish({id, title, description, price, category, view_access, currency}) {
        return HttpService.put(`wishes/${id}/`, {
            title,
            description,
            price,
            category,
            view_access,
            currency
        });
    }

    static async deleteWish({id}) {
        return HttpService.delete(`wishes/${id}/`);
    }
}