import HttpService from "~/services/http.service";

export default class UsersApiService {
    static async getUsers({search, ordering}) {
        return HttpService.get(`users`, {
            params: {
                search,
                ordering
            }
        });
    }

    static async getUsersId({id}) {
        return HttpService.get(`users/${id}`);
    }

    static async getUsersWishList({id}) {
        return HttpService.get(`users/${id}/wishlist`);
    }

    static async putUsersById({id, username, first_name, last_name, gender, age}) {

        return HttpService.put(`users/${id}`, {
            username,
            first_name,
            last_name,
            age,
            gender
        });
    }

    static async  deleteUsersById({id}) {
        return HttpService.delete(`users/${id}`
        )
    }
}