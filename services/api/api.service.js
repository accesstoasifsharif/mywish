import TagsApiService from "./tags.api.service";
import UsersApiService from "~/services/api/users.api.service";
import AuthApiService from "~/services/api/auth.api.service";
import WishApiService from "~/services/api/wish.api.service";

class ApiService {
    constructor() {
        this.tags = TagsApiService;
        this.users = UsersApiService;
        this.auth = AuthApiService;
        this.wish = WishApiService;
    }
}

export default new ApiService();