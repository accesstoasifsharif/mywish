import api from "~/services/api/api.service";

export default class WishService {
    constructor(store) {
        this.store = store;
    }

    async createWish(wish) {
        const data = await api.wish.addWish(wish);

        if (!data) return;
    }
}