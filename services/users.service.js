import api from "~/services/api/api.service";
import HttpService from "@/services/http.service";

export default class UsersService {
    constructor(store) {
        this.store = store;
    }

    async fetchUsers(id) {
        const data = await api.users.getUsersId({id});
        if (!data) return;
        await this.store.dispatch('user/getUser', data.data);
    }

    async getOwnUserList(id) {
        const data = await api.users.getUsersWishList({id});

        if (!data) return;

        await this.store.dispatch('user/getOwnUserList', data.data);
    }

    async updateUserData(userData) {
        const data = await api.users.putUsersById(userData);
        if (!data) return;

        await this.store.dispatch('user/getUser');
    }
}