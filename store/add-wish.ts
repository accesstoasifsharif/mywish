import {ActionTree, GetterTree, MutationTree} from "vuex";
import axios from "axios";

export const state = () => ({
    status: '',
    wish: {}
});


const headers = {
    'Content-Type': 'application/json',
}

export const actions = {
    POST_WISH(context, credential) {
        return axios.post('http://18.193.224.125/api/wishes/', {
            wish: credential.wish,
        }, {headers: headers})
            .then(response => {
                context.commit('UPDATE_WISH', response.data.tokens);
            })
            .catch(e => {
                console.log('login error: ', e);
            });
    },
};

export const mutations = {
    ADD_WISH: (state, response: object) => {
        // console.log('mutation for wish card', state, response);
        // state.tokens = response;
        // state.tokens ? state.status = 'isLogin' : '';
        // localStorage.setItem('atoken', JSON.stringify(state.tokens));
        // localStorage.setItem('isLogin', state.status);
    }

};

export const getters = {

};
