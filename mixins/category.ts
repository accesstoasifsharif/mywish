import {Vue, Component, Watch} from "nuxt-property-decorator";

@Component({})

export default class CategoryMixin extends Vue {
    tagsList = [];
    sliderCategoryList = [];
    currentCategory = [];
    wishList = [];

    created() {
        this.sliderCategoryList = [
            {
                name: "Уход за собой",
                id: "1"
            },
            {
                name: "Хобби и творчество",
                id: "2"
            },
            {
                name: "Всё для дома и сада",
                id: "3"
            },
            {
                name: "Техника, электроника",
                id: "4"
            },
            {
                name: "Хобби творчество",
                id: "5"
            },
            {
                name: "Книги",
                id: "6"
            }
        ];
        this.currentCategory = [
            {
                name: "lol",
                id: "666"
            },
            {
                name: "lol 2",
                id: "667"
            },
            {
                name: "lol 3",
                id: "668"
            }
        ];

    }

    updateCurrentList(wish, event) {
        event.target.checked === true ?
            this.currentCategory = [...this.currentCategory, wish] :
            this.currentCategory = this.deleteItemFromArray(this.currentCategory, wish);
    }

    deleteItemFromArray(arr, deleteItem) {
        return arr.filter(item => item.id !== deleteItem.id);
    }

    deleteTag(tag) {
        this.currentCategory = this.deleteItemFromArray(this.currentCategory, tag);
        this.tagsList = this.deleteItemFromArray(this.tagsList, tag);
    }
}
